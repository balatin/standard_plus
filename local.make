; standard_plus make file for local development
core = "7.27"
api = "2"

projects[drupal][version] = "7.27"
; include the d.o. profile base
includes[] = "https://bitbucket.org/balatin/standard_plus/raw/master/drupal-org.make"

; +++++ Libraries +++++

# ; jquery.cycle
# libraries[jquery.cycle][directory_name] = "jquery.cycle"
# libraries[jquery.cycle][type] = "library"
# libraries[jquery.cycle][destination] = "libraries"
# libraries[jquery.cycle][download][type] = "get"
# libraries[jquery.cycle][download][url] = "https://github.com/malsup/cycle/archive/master.zip";

# ; modernizr
# libraries[modernizr][directory_name] = "modernizr"
# libraries[modernizr][type] = "library"
# libraries[modernizr][destination] = "libraries"
# libraries[modernizr][download][type] = "get"
# libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/master.zip";

